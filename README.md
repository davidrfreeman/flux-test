# Flux Demos

Intent of this project is to have a basic helm chart and a flux directory to allow for installing the chart via flux.

## TODOs

* Figure out how to best install flux in a cluster
  * `flux install --namespace=flux-system`
* Could provide a k3d cluster config
* Create helm chart from scratch or select existing chart
